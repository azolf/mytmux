+++
title = "Users Configurations"
description = ""
weight = 5
+++

You could put your configuration file under mytmux.com domain.
Firstly, it lets other users to see configuration files and use them as a sample to build their own.
Secondly, it's always available online. If you start to work on a new system which doesn't have your tmux configuration, run command below to get it.

```
$ curl http://mytmux.com/YOUR_USERNAME.conf > ~/.tmux.conf.sample
```

### How submit your file?
You could just clone this project, and add your configuration to static files and then create a merge request for it. When the merge request is merged, it will be always available.
```
$ git clone https://gitlab.com/azolf/mytmux.git
$ cd mytmux/
$ git checkout -b YOUR_USERNAME
$ touch static/YOUR_USERNAME.conf
```
edit *static/YOUR_USERNAME.conf* file and save your configuration file.

```
$ git add static/YOUR_USERNAME.conf
$ git commit -m "added YOUR_USERNAME tmux configuration"
$ git push -u YOUR_USERNAME
```

# Available configuration files

{{%configfiles %}}
