+++
title = "Installation"
description = "How to install tmux?"
weight = 1
+++

### Dependencies

tmux depends on [libevent 2.x](https://libevent.org/), available from [this page](https://github.com/libevent/libevent/releases/latest)

It also depends on [ncurses](https://www.gnu.org/software/ncurses/), available from [this page](https://invisible-mirror.net/archives/ncurses/)

### Installation

##### To build and install tmux from a release tarball, use:
```bash
$ ./configure && make
$ sudo make install
```
tmux can use the utempter library to update utmp(5), if it is installed - run
configure with --enable-utempter to enable this.

##### To get and build the latest from version control
{{% alert theme="warning" %}}
note that this requires autoconf, automake and pkg-config:
{{% /alert %}}
```bsah
$ git clone https://github.com/tmux/tmux.git
$ cd tmux
$ sh autogen.sh
$ ./configure && make
```

##### To get from your package manager

Installing tmux on Ubuntu and Debian
```bash
$ sudo apt install tmux
```

Installing tmux on CentOS and Fedora
```bash
$ sudo yum install tmux
```

Installing tmux on macOS
```bash
$ brew install tmux
```
