+++
title = "Options"
description = ""
weight = 2
+++

From https://linux.die.net/man/1/tmux

The appearance and behaviour of tmux may be modified by changing the value of various options. There are three types of option: server options, session options and window options.

The tmux server has a set of global options which do not apply to any particular window or session. These are altered with the **set-option -s** command, or displayed with the **show-options -s** command.

In addition, each individual session may have a set of session options, and there is a separate set of global session options. Sessions which do not have a particular option configured inherit the value from the global session options. Session options are set or unset with the **set-option** command and may be listed with the **show-options** command. The available server and session options are listed under the **set-option** command.

Similarly, a set of window options is attached to each window, and there is a set of global window options from which any unset options are inherited. Window options are altered with the **set-window-option** command and can be listed with the **show-window-options** command. All window options are documented with the set-window-option command.

Commands which set options are as follows:
```
set-option [-agsuw] [-t target-session | target-window] option value
```

(alias: set) Set a window option with -w (equivalent to the set-window-option command), a server option with -s, otherwise a session option.
If -g is specified, the global session or window option is set. With -a, and if the option expects a string, value is appended to the existing setting. The -u flag unsets an option, so a session inherits the option from the global options. It is not possible to unset a global option.

### Available window options are listed under *set-window-option*

##### Available server options are:

**buffer-limit number**

Set the number of buffers; as new buffers are added to the top of the stack, old ones are removed from the bottom if necessary to maintain this maximum length.

**set-clipboard [on | off]**

Attempt to set the terminal clipboard content using the \e]52;...\007 xterm(1) escape sequences. This option is on by default if there is an Ms entry in the terminfo(5) description for the client terminal. Note that this feature needs to be enabled in xterm(1) by setting the resource:

disallowedWindowOps: 20,21,SetXprop

Or changing this property from the xterm(1) interactive menu when required.
```
escape-time time
```
Set the time in milliseconds for which tmux waits after an escape is input to determine if it is part of a function or meta key sequences. The default is 500 milliseconds.

**exit-unattached [on | off]**

If enabled, the server will exit when there are no attached clients.

**quiet [on | off]**

Enable or disable the display of various informational messages (see also the -q command line flag).

##### Available session options are:

**base-index index**

Set the base index from which an unused index should be searched when a new window is created. The default is zero.

**bell-action [any | none | current]**

Set action on window bell. any means a bell in any window linked to a session causes a bell in the current window of that session, none means all bells are ignored and current means only bell in windows other than the current window are ignored.

**bell-on-alert [on | off]**

If on, ring the terminal bell when an activity, content or silence alert occurs.

**default-command shell-command**

Set the command used for new windows (if not specified when the window is created) to shell-command, which may be any sh(1) command. The default is an empty string, which instructs tmux to create a login shell using the value of the default-shell option.

**default-path path**

Set the default working directory for processes created from keys, or interactively from the prompt. The default is empty, which means to use the working directory of the shell from which the server was started if it is available or the user's home if not.

**default-shell path**

Specify the default shell. This is used as the login shell for new windows when the default-command option is set to empty, and must be the full path of the executable. When started tmux tries to set a default value from the first suitable of the SHELL environment variable, the shell returned by getpwuid(3), or /bin/sh. This option should be configured when tmux is used as a login shell.

**default-terminal terminal**

Set the default terminal for new windows created in this session - the default value of the TERM environment variable. For tmux to work correctly, this must be set to 'screen' or a derivative of it.

**destroy-unattached [on | off]**

If enabled and the session is no longer attached to any clients, it is destroyed.

**detach-on-destroy [on | off]**

If on (the default), the client is detached when the session it is attached to is destroyed. If off, the client is switched to the most recently active of the remaining sessions.

**display-panes-active-colour colour**

Set the colour used by the display-panes command to show the indicator for the active pane.

**display-panes-colour colour**

Set the colour used by the display-panes command to show the indicators for inactive panes.

**display-panes-time time**

Set the time in milliseconds for which the indicators shown by the display-panes command appear.

**display-time time**

Set the amount of time for which status line messages and other on-screen indicators are displayed. time is in milliseconds.

**history-limit lines**

Set the maximum number of lines held in window history. This setting applies only to new windows - existing window histories are not resized and retain the limit at the point they were created.

**lock-after-time number**

Lock the session (like the lock-session command) after number seconds of inactivity, or the entire server (all sessions) if the lock-server option is set. The default is not to lock (set to 0).

**lock-command shell-command**

Command to run when locking each client. The default is to run lock(1) with -np.

**lock-server [on | off]**

If this option is on (the default), instead of each session locking individually as each has been idle for lock-after-time, the entire server will lock after all sessions would have locked. This has no effect as a session option; it must be set as a global option.

**message-attr attributes**

Set status line message attributes, where attributes is either none or a comma-delimited list of one or more of: bright (or bold), dim, underscore, blink, reverse, hidden, or italics.

**message-bg colour**

Set status line message background colour, where colour is one of: black, red, green, yellow, blue, magenta, cyan, white, colour0 to colour255 from the 256-colour set, default, or a hexadecimal RGB string such as '#ffffff', which chooses the closest match from the default 256-colour set.

**message-fg colour**

Set status line message foreground colour.

**message-limit number**

Set the number of error or information messages to save in the message log for each client. The default is 20.

**mouse-resize-pane [on | off]**

If on, tmux captures the mouse and allows panes to be resized by dragging on their borders.

**mouse-select-pane [on | off]**

If on, tmux captures the mouse and when a window is split into multiple panes the mouse may be used to select the current pane. The mouse click is also passed through to the application as normal.

**mouse-select-window [on | off]**

If on, clicking the mouse on a window name in the status line will select that window.

**pane-active-border-bg colour**

**pane-active-border-fg colour**

Set the pane border colour for the currently active pane.

**pane-border-bg colour**

**pane-border-fg colour**
Set the pane border colour for panes aside from the active pane.

**prefix keys**

Set the keys accepted as a prefix key. keys is a comma-separated list of key names, each of which individually behave as the prefix key.

**repeat-time time**

Allow multiple commands to be entered without pressing the prefix-key again in the specified time milliseconds (the default is 500). Whether a key repeats may be set when it is bound using the -r flag to bind-key. Repeat is enabled for the default keys bound to the resize-pane command.

**mouse-utf8 [on | off]**

If enabled, request mouse input as UTF-8 on UTF-8 terminals.

**set-remain-on-exit [on | off]**

Set the remain-on-exit window option for any windows first created in this session. When this option is true, windows in which the running program has exited do not close, instead remaining open but inactivate. Use the respawn-window command to reactivate such a window, or the kill-window command to destroy it.

**set-titles [on | off]**

Attempt to set the window title using the \e]2;...\007 xterm code if the terminal appears to be an xterm. This option is off by default. Note that elinks will only attempt to set the window title if the STY environment variable is set.

**set-titles-string string**

String used to set the window title if set-titles is on. Character sequences are replaced as for the status-left option.

**status [on | off]**

Show or hide the status line.

**status-attr attributes**

Set status line attributes.

**status-bg colour**

Set status line background colour.

**status-fg colour**

Set status line foreground colour.

**status-interval interval**

Update the status bar every interval seconds. By default, updates will occur every 15 seconds. A setting of zero disables redrawing at interval.

**status-justify [left | centre | right]**

Set the position of the window list component of the status line: left, centre or right justified.

**status-keys [vi | emacs]**

Use vi or emacs-style key bindings in the status line, for example at the command prompt. The default is emacs, unless the VISUAL or EDITOR environment variables are set and contain the string 'vi'.

**status-left string**

Display string to the left of the status bar. string will be passed through strftime(3) before being used. By default, the session name is shown. string may contain any of the following special character sequences:
