+++
title = "Working with tmux"
description = ""
weight = 2
pre ="<i class='fa fa-terminal' ></i> "

+++

The main trick for using tmux, is the **prefix key** for its all commands.

### What is the prefix key?
the prefix is **C-b** by default.

#### Hah? what does that mean?
- the **C-** means *press and hold the `Ctrl` key*

- the **b** means *press the b key*

now it makes sense, you need to hold the **Ctrl** key and then press the **b** button. this is just the prefix, after that you could press different shortcuts.
here is a simple shortcut for creating a new window in current session.

```
C-b c
```

do you get that?
press and hold the **Ctrl** key and then press **b**, after that release the **Ctrl** key and immediately press the **c** key. Now you should see two windows in your status bar like below.

![tmux with 2 windows](/images/tmux-with-2-windows.png)

Now it's time to move on and learn tricky shortcuts.
