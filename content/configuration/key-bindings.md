+++
title = "Key Bindings"
description = ""
weight = 1
+++

From https://linux.die.net/man/1/tmux

*tmux* allows a command to be bound to most keys, with or without a prefix key. When specifying keys, most represent themselves (for example 'A' to 'Z'). Ctrl keys may be prefixed with 'C-' or '^', and Alt (meta) with 'M-'. In addition, the following special key names are accepted: Up, Down, Left, Right, BSpace, BTab, DC (Delete), End, Enter, Escape, F1 to F20, Home, IC (Insert), NPage (Page Up), PPage (Page Down), Space, and Tab. Note that to bind the '"' or ''' keys, quotation marks are necessary, for example:

bind-key '"' split-window
bind-key "'" new-window
Commands related to key bindings are as follows:

```
bind-key [-cnr] [-t key-table] key command [arguments]
```

(alias: **bind**) Bind key key to command. By default (without -t) the primary key bindings are modified (those normally activated with the prefix key); in this case, if -n is specified, it is not necessary to use the prefix key, command is bound to key alone. The -r flag indicates this key may repeat, see the repeat-time option.
If -t is present, key is bound in key-table: the binding for command mode with -c or for normal mode without. To view the default bindings and possible commands, see the list-keys command.

**list-keys [-t key-table]**

(alias: lsk) List all key bindings. Without -t the primary key bindings - those executed when preceded by the prefix key - are printed. Keys bound without the prefix key (see bind-key -n) are marked with '(no prefix)'.

With -t, the key bindings in key-table are listed; this may be one of: vi-edit, emacs-edit, vi-choice, emacs-choice, vi-copy or emacs-copy.

**send-keys [-t target-pane] key ...**

(alias: send) Send a key or keys to a window. Each argument key is the name of the key (such as 'C-a' or 'npage' ) to send; if the string is not recognised as a key, it is sent as a series of characters. All arguments are sent sequentially from first to last.

**send-prefix [-t target-pane]**

Send the prefix key to a window as if it was pressed. If multiple prefix keys are configured, only the first is sent.

**unbind-key [-acn] [-t key-table] key**

(alias: unbind) Unbind the command bound to key. Without -t the primary key bindings are modified; in this case, if -n is specified, the command bound to key without a prefix (if any) is removed. If -a is present, all key bindings are removed.

If -t is present, key in key-table is unbound: the binding for command mode with -c or for normal mode without.
