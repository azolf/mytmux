+++
title = "Scroll"
description = ""
weight = 4

+++
Scrolling in tmux is tricky. There is a copy mode in tmux which you should get into first, and then you could scroll over your pane.

First you need the shortcut below for entring into copy mode.
```
C-b [
```
Then you could use arrow or Page-Up/Page-down keys to scroll over the pane.
