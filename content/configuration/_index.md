+++
title = "Configuration"
description = ""
weight = 3
+++

tmux has a configuration file which let's you change so many things.
It lets you change the shortcuts, colors, etc.

The default address for tmux configuration file is **~/.tmux.conf**

You could pass the configuration file like below
```
$ tmux -f FILE_ADDRESS
```

let's take a look at a simple config file.
```
# Set status line background colour.
set-option -g status-bg red

# Display string (by default the session name) to the left of the
# status line.  string will be passed through strftime(3).  Also
# see the FORMATS and STYLES sections.
# 
# For details on how the names and titles can be set see the
# NAMES AND TITLES section.
# 
# Examples are:
# 
#       #(sysctl vm.loadavg)
#       #[fg=yellow,bold]#(apm -l)%%#[default] [#S]

# The default is ‘[#S] ’.
set-option -g status-left " #h |"

# Set the maximum length of the left component of the status
#             line.  The default is 10.
set-option -g status-left-length 20

# Display string to the right of the status bar. By default, the current window
# title in double quotes, the date and the time are shown. As with status-left,
# string will be passed to strftime(3), character pairs are replaced, and UTF-8
# is dependent on the status-utf8 option.
set-option -g status-right "<#S> #(date +'%H:%M %d-%h-%Y')"

# Set the maximum number of lines held in window history. This setting applies
# only to new windows - existing window histories are not resized and retain the
# limit at the point they were created.
set -g history-limit 10000
```

{{%children style="h2" description="true"%}}
