+++
title = "Panes"
description = ""
weight = 1

+++

Each window of tmux, which is each tab you could see in your status bar, could have multiple panes. You could split the current window in 2 different panes.

### How it's use full?
You could have a window which is following different log files.
Check this out.
![tmux with 2 panes](/images/tmux-with-2-panes.png)

You could extend the panes and split each pane and create something like this.
![tmux with multiple panes](/images/tmux-with-multiple-panes.png)

Now let's take a look at panes shortcuts

| Shortcuts      | Description |
| ----------- | ----------- |
| C-b ;      | Toggle last active pane |
| C-b %   | Split pane vertically |
| C-b " | Split pane horizontally |
| C-b { | Move the current pane left |
| C-b } | Move the current pane right |
| C-b up | Switch to pane to the up |
| C-b down | Switch to pane to the down |
| C-b left | Switch to pane to the left |
| C-b right | Switch to pane to the right |
| C-b q | Show pane numbers |
| C-b q 0...9 | Switch/select pane by number |
| C-b z | Toggle pane zoom |
| C-b ! | Convert pane into a window |
| C-b C-up | Resize current pane height(holding second key is optional) |
| C-b C-down | Resize current pane height(holding second key is optional) |
| C-b C-left | Resize current pane height(holding second key is optional) |
| C-b C-right | Resize current pane height(holding second key is optional) |
| C-b x | Close current pane |
| C-b o | Switch to next pane |
| C-b space | Toggle between pane layouts |
