+++
title = "Windows"
description = ""
weight = 2

+++

Creating panes is not enough, we need different windows for different things. It's up to you how to organize and separate your windows.

| Shortcuts      | Description |
| ----------- | ----------- |
| C-b c | Create window |
| C-b , | Rename current window |
| C-b & | Close current window |
| C-b n | Next window |
| C-b p | Previous window |
| C-b 0..9 | Switch/select window by number |
| C-b ' | Prompt for a window index to select |
| C-b w | Choose the current window interactively |
