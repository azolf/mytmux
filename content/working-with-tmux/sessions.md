+++
title = "Sessions"
description = ""
weight = 3
+++

How many windows you could create? it's unlimited but it's confusing.
For instance I'm a system administrator who maintain 45 servers of company. We have 6 different applications and 10 database. It's possible to have a 45 window in a session with different names, but honestly that would not work.

### What's the solution?
You could have create different sessions and each of them of have the related windows for that session.
For example You could create a session for each application. Each session have different windows for that application like logs, databases, etc.

When you run tmux command it creates a new session. Run command below to see all the sessions you have in tmux.
```
$ tmux ls
```

But this is not the only advantage of sessions in tmux, the most important feature is that you could detach from a session and come back to it later.
So for a system administrator for example which needs to have some windows always open, this is the best.
She could create different sessions, and each time she login to that server, she needs to attach to her old tmux session and everything is still there.

Awesome?

Let's take a look at sessions shortcuts.

#### Start a new session
```
$ tmux
```
```
$ tmux new
```
```
$ tmux new-session
```
 if you are in a tmux session already, and needs to create a session here is the shortcut
 ```
 C-b :new
 ```
We will talk about tmux command mode later.

#### Show all sessions
```
$ tmux ls
```
```
$ tmux list-sessions
```
if you are in a tmux session already
```
C-b s
```
You could navigate to your session and press enter for going into that session or choose that session index which is in **()** before the session name.

#### Detach from session
```
C-b d
```

#### Rename session
```
C-b $
```

#### Attach to last session
```
$ tmux a
```
```
$ tmux at
```
```
$ tmux attach
```
```
$ tmux attach-session

```

#### Attach to a session with the name mysession
```
$ tmux a -t mysession
```
```
$ tmux at -t mysession
```
```
$ tmux attach -t mysession
```
```
$ tmux attach-session -t mysession

```

#### Move to previous session

```
C-b (
```

#### Move to Next session

```
C-b )
```

### Anything else for sessions?
Yup, it's not finished !

There is another cool feature which lets different persons attach to a same session and share it.

#### How could it be useful?
For instance, If you need help from a person who is not around, she could login to same server and attach to your session. Now instead of chatting and doing copy paste, she could write commands and you could see it.
