+++
title = "Your First Session"
description = "Start your first session"
weight = 2
+++

it's super easy, open your terminal and run this
```
$ tmux
```
The status bar at the bottom is the most important part of it.

![simple tmux](/images/simple-tmux.png?classes=shadow)
