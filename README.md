## What is [tmux?](https://github.com/tmux/tmux)
> tmux is a terminal multiplexer: it enables a number of terminals to be created, accessed, and controlled from a single screen.

> tmux may be detached from a screen and continue running in the background, then later reattached.

## What is this website?
because the tmux documentation is a little confusing, we created this website to simplify it. You could learn how to install, setup and use tmux here, moreover, we have a list of different people's configuration for their tmux which is available [here](https://mytmux.com/users-configurations/)

## When should I use tmux?
if you are a person who works with terminal mostly or having servers to maintain, you should work with tmux.
